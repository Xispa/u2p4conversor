package dam.androidIgnacio.u2p4conversor;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class U2P4ConversorActivity extends LogActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    /**
     * Métode per a carregar la vista general
     */
    private void setUI() {
        final TextView etError = findViewById(R.id.et_Error);
        etError.setVisibility(View.INVISIBLE);
        clickButtonPulgadas();
        clickButtonCentimetros();
    }


    /**
     * Mètode per a l'event de clic del botó de conversió de polçades
     */
    private void clickButtonPulgadas() {
        final EditText etPulgada = findViewById(R.id.et_Pulgada);
        final EditText etResultado = findViewById(R.id.et_Resultado);
        final TextView etError = findViewById(R.id.et_Error);

        Button buttonConvertirPulgadas = findViewById(R.id.button_Convertir_In);
        buttonConvertirPulgadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    // TODO Ex2 Restringir nums < 1
                    double d = Double.parseDouble(etPulgada.getText().toString());
                    if (d >= 1) {
                        etResultado.setText(convertirPulgadas(etPulgada.getText().toString()));
                        etError.setVisibility(View.INVISIBLE);
                        // TODO Ex3 Logs
                        Log.i("LogsConversor", "Botón Pulgadas > Cm pulsado");
                    } else {
                        etError.setVisibility(View.VISIBLE);
                        Log.i("LogsConversor", "Botón Pulgadas > Cm pulsado");
                        throw new Exception();
                    }
                } catch (Exception e) {
                    Log.e("LogsConversor", "Sólo números >=1");
                }
            }
        });

    }

    /**
     * Mètode per a l'event de clic del botó de conversió de centímetres
     */
    private void clickButtonCentimetros() {

        final EditText etPulgada = findViewById(R.id.et_Pulgada);
        final EditText etResultado = findViewById(R.id.et_Resultado);
        final TextView etError = findViewById(R.id.et_Error);

        Button buttonConvertirCentimetros = findViewById(R.id.button_Convertir_Cm);
        buttonConvertirCentimetros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    // TODO Ex2 Restringir nums < 1
                    double d = Double.parseDouble(etPulgada.getText().toString());
                    if (d >= 1) {
                        etResultado.setText(convertirCentimetros(etPulgada.getText().toString()));
                        etError.setVisibility(View.INVISIBLE);
                        // TODO Ex3 Logs
                        Log.i("LogsConversor", "Botón Cm > Pulgadas pulsado");
                    } else {
                        etError.setVisibility(View.VISIBLE);
                        Log.i("LogsConversor", "Botón Cm > Pulgadas pulsado");
                        throw new Exception();
                    }
                } catch (Exception e) {
                    Log.e("LogsConversor", "Sólo números >=1");
                }
            }
        });

    }

    /**
     * Mètode de conversió de polçades
     * @param centimetroText String
     * @return String
     */
    private String convertirPulgadas(String centimetroText) {
        // TODO Ex1 Formato
        DecimalFormat format = new DecimalFormat("#.##");
        double centimetroValue = Double.parseDouble(centimetroText) / 2.54;
        return format.format(centimetroValue) + " cm";
    }

    /**
     * Mètode de conversió de centímetres
     * @param pulgadaText String
     * @return String
     */
    private String convertirCentimetros(String pulgadaText) {
        // TODO Ex1 Formato
        DecimalFormat format = new DecimalFormat("#.##");
        double pulgadaValue = Double.parseDouble(pulgadaText) * 2.54;
        return format.format(pulgadaValue) + "\"";
    }

}