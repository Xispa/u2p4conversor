package dam.androidIgnacio.u2p4conversor;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

// TODO Ex3 Logs
public class LogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("LogsConversor", "onCreate");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("LogsConversor", "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("LogsConversor", "onPause");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("LogsConversor", "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("LogsConversor", "onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("LogsConversor", "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("LogsConversor", "onStop");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.i("LogsConversor", "Back button pressed");
    }

}
